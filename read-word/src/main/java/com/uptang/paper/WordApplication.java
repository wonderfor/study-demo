package com.uptang.paper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;

/**
 *
 * @author cht
 * @version 4.2.0
 * @date 2020-07-15
 */
@Controller
@SpringBootApplication
public class WordApplication {

    public static void main(String[] args) {
        SpringApplication.run(WordApplication.class, args);
    }

}
